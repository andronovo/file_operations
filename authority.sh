#!/bin/bash

CHK[1]=/tmp/chk1
CHK[2]=/tmp/chk2
CHK[3]=/tmp/chk3
CHK[4]=/tmp/chk4

sudo_allowed() { sudo -nl "$@" || (sudo -v && sudo -l "$@" &>/dev/null) && return 0; }
run_chmod_command() { if [[ `sudo stat -c '%a' $line` != $EDIT_KOD ]]; then
sudo chmod -c "$EDIT_KOD" "${line}" 2>>${CHK[3]}; fi; }
run_chown_command() { if [[ "`sudo stat -c '%u %g' $line`" != "`sudo stat -c '%u %g' /tmp/group_test`" ]]; then
sudo chown -c "$EDIT_GROUP" "${line}" 2>>${CHK[3]}; fi; }
edit_file_info() { echo -e ""${MSG[31]}" `date +%D'  '%T`\n\n" >>${CHK[4]}
sudo stat $EDIT_DIRS | head -n7 | tee >>${CHK[4]}
echo -e "${MSG[30]}"; sleep 2
[[ -e ${CHK[3]} ]] && rm -f ${CHK[3]}; }
create_all_set() { clear; edit_file_info
while IFS= read -r line; do
export line; echo >>${CHK[4]}
echo -e "eski \t `sudo stat -c '%a %u %g' $line`  `echo $line | \
sed s/.*$BASE_NAME//`" >>${CHK[4]}
  if [[ `sudo ls -ld "$line" 2>/dev/null | grep "^$sel"` != "" ]]; then
     [[ ! -z $EDIT_KOD ]] && run_chmod_command
     [[ ! -z $EDIT_GROUP ]] && run_chown_command
  fi
echo -e "yeni \t `sudo stat -c '%a %u %g' $line`  `echo $line | \
sed "s/.*$BASE_NAME//"`" >>${CHK[4]}
done <<< `sudo find "$EDIT_DIRS"`
if [[ `grep '[a-z]' ${CHK[3]} 2>/dev/null` != "" ]]; then clear
echo -e "${MSG[29]}"
sleep 3; cat ${CHK[3]}; fi
sudo bash -c "mv ${CHK[4]} $EDIT_DIRS/$BASE_NAME-authority_log.txt"
sudo chmod 400 $EDIT_DIRS/$BASE_NAME-authority_log.txt
sudo chown root:root $EDIT_DIRS/$BASE_NAME-authority_log.txt
echo -ne "${SLCT[11]}"; read -p "" FINAL; clear
[[ $FINAL == 'b' ]] && (clear; sudo cat $EDIT_DIRS/$BASE_NAME-authority_log.txt; \
exit 0) || exit 0; }
see_old_set() { while IFS= read -r line; do
echo -e "${CB} `sudo stat -c '%a %u %g' $line` \t `echo $line | sed s/.*$BASE_NAME//`" 
done <<< `sudo find "$EDIT_DIRS"`
echo -e "${SLCT[10]}"; read ok; }
select_xsel() { clear; echo -e "${MSG[28]} ${CT}$EDIT_DIRS/$BASE_NAME-authority_log.txt"
content_info; echo -e "${RCSS[1]} $CHMD \n${RCSS[1]} $CHWN"
echo -ne "${SLCT[9]}"; read -p "" ASSENT
  if [[ `echo $ASSENT` -eq 1 ]]; then sel='d'; create_all_set
elif [[ `echo $ASSENT` -eq 3 ]]; then sel='-'; create_all_set
elif [[ `echo $ASSENT` -eq 5 ]]; then sel=""; create_all_set
elif [[ `echo $ASSENT` == 'h' ]]; then exit 0
else clear; echo -e "${MSG[27]}"; sleep 2; clear; select_xsel; fi; }
action_selections() {
IGNORE='/bin\|/dev\|/lib64\|/root\|/sbin\|/sys\|/usr\|/boot\|/etc\|/lib\|/proc\|/run\|/srv'
if [[ `echo $EDIT_DIRS | awk -F/ '{print "/" $2}' | grep "$IGNORE"` != "" ]]; then content_info
echo -e "${MSG[25]} `echo $EDIT_DIRS | awk -F/ '{print "/" $2}'` ${MSG[26]}"; echo -ne "${SLCT[6]}"
read -p "" SEE ; [[ `echo $SEE` = 'e' ]] && (clear; see_old_set); echo -ne "${SLCT[7]}"
read -p "" SLC ; [[ `echo $SLC` = 'y' ]] && (clear; select_dir_name) || exit 0
else clear; content_info; echo -e "${RCSS[1]} $CHMD \n${RCSS[1]} $CHWN"
echo -ne "${SLCT[8]}"; read -p "" SEE
[[ `echo $SEE` = 'e' ]] && (clear; see_old_set) || clear; select_xsel ; fi; }
check_all_set() { [[ -z "$EDIT_GROUP" ]] && CHWN="${MSG[23]} ${CT}${OLD_GROUP} ${CB} > ${MSG[15]}" || \
CHWN="${MSG[23]}  ${CT}${OLD_GROUP} ${CB} > $EDIT_GROUP  ${MSG[16]}"
if [[ ! -z $EDIT_KOD ]] || [[ ! -z $EDIT_GROUP ]]; then action_selections; else sudo ls -l --color $EDIT_DIRS ; fi; }
check_edit_groups() { mkdir /tmp/group_test 2>/dev/null
if sudo chown -R $EDIT_GROUP /tmp/group_test 2>/dev/null; then check_all_set
sudo rm -rf /tmp/group_test 2>/dev/null
else echo -e "${RCSS[1]} $EDIT_GROUP ${MSG[24]}"
sudo chown -R $EDIT_GROUP /tmp/group_test; echo -ne "${SLCT[3]}"
read -p "" ANSWER ; sudo rm -rf /tmp/group_test 2>/dev/null
if [[ $ANSWER = 'c' ]]; then clear; exit 0; else clear; select_edit_groups; fi; fi; }
select_edit_groups() { [[ -z "$EDIT_KOD" ]] && CHMD="${MSG[14]} ${CT}`sudo stat -c '%a' $EDIT_DIRS` \
${CB} > ${MSG[15]}" || CHMD="${MSG[14]}  ${CT}`sudo stat -c '%a' $EDIT_DIRS` ${CB} > $EDIT_KOD  ${MSG[16]}"
OLD_GROUP=`sudo ls -l $(echo $EDIT_DIRS | rev | cut -d/ -f2- | rev) | \
grep -w "$BASE_NAME$" | awk '{print $3,$4}'`
clear; content_info; echo -e "${RCSS[1]} $CHMD ${MSG[17]}"
echo -e "\n ${MSG[18]}  ${CT}`cut -d: -f1 /etc/group | xargs` ${MSG[19]}"
echo -ne "${RCSS[1]}  ${MSG[12]} ${CB}${OLD_GROUP} ${SLCT[5]}"
read -p "" EDIT_GROUP ; clear; eval EDIT_GROUP=`echo $EDIT_GROUP`
[[ -z $EDIT_GROUP ]] && check_all_set || check_edit_groups; }
check_edit_kod() { FAIL=""
[ `echo ${EDIT_KOD:0:1} | grep '[0-7]'` ] || FAIL=1
[ `echo ${EDIT_KOD:1:1} | grep '[0-7]'` ] || FAIL=1
[ `echo ${EDIT_KOD:2:1} | grep '[0-7]'` ] || FAIL=1
if [[ ${#EDIT_KOD} -eq 3 ]] && [[ ${FAIL} -ne 1 ]]; then
select_edit_groups; else echo -ne "\n $EDIT_KOD ${MSG[13]} ${SLCT[3]}"; read -p "" answer
if [[ $answer = 'c' ]]; then clear; exit 0; else clear; select_edit_kod; fi; fi; }
select_edit_kod() { clear; content_info; echo -e "${MSG[11]}"
echo -ne "${RCSS[1]}   ${MSG[12]} ${CB}`sudo stat -c '%a' $EDIT_DIRS` \
${SLCT[4]}"; read -p "" EDIT_KOD ; clear
[[ -z $EDIT_KOD ]] && select_edit_groups || check_edit_kod; }
content_info() { echo -e "${RCSS[1]} ${CB}$EDIT_DIRS  ${MSG[6]} ${CB}$DIR_PIECE ${MSG[7]}  \
${CB}$FILE_PIECE ${MSG[8]}  ${CB}$LINK_PIECE ${MSG[9]} ${CB}$TOTAL_PIECE ${MSG[10]}"; }
dir_content_info() { clear; echo -e "${MSG[5]}"
DIR_PIECE=0 ; FILE_PIECE=0 ; LINK_PIECE=0
while IFS= read -r line; do
[[ `sudo ls -ld "$line" 2>/dev/null | grep -o '^d'` == 'd' ]] && \
let DIR_PIECE=DIR_PIECE+1
[[ `sudo ls -ld "$line" 2>/dev/null | grep -o '^-'` == '-' ]] && \
let FILE_PIECE=FILE_PIECE+1
[[ `sudo ls -ld "$line" 2>/dev/null | grep -o '^l'` == 'l' ]] && \
let LINK_PIECE=LINK_PIECE+1
done <<< `sudo find "$EDIT_DIRS"` ; clear
TOTAL_PIECE=`expr $DIR_PIECE + $FILE_PIECE + $LINK_PIECE`; }
prohibited_area() { if [[ `echo "$EDIT_DIRS" | grep -x "$(ls /|sed 's/^/\//g')"` != "" ]]; then
clear; echo -ne "\n ${EDIT_DIRS} ${MSG[4]} ${SLCT[3]} "; read -p "" answer
if [[ $answer = 'c' ]]; then clear; exit 0; else clear; select_dir_name; fi; fi; }
check_dir_name() { if [ -d $EDIT_DIRS ]; then prohibited_area; dir_content_info;
export BASE_NAME=`basename $EDIT_DIRS`; select_edit_kod
else echo -ne "\n ${EDIT_DIRS} ${MSG[3]} ${SLCT[3]} "; read -p "" answer
if [[ $answer = 'c' ]]; then clear; exit 0; else clear; select_dir_name; fi; fi; }
select_multi_dir() { local IGNORE='/dev\|/root\|/sys\|/boot\|/proc'
rm ${CHK[2]} 2>/dev/null; for dir in `cat ${CHK[1]} 2>/dev/null | grep -v "$IGNORE"`; do
test -d $dir && echo $dir >>${CHK[2]}; done
if [[ `grep -c '/[a-z0-9]' ${CHK[2]} 2>/dev/null` -eq 1 ]]; then 
EDIT_DIRS=`cat ${CHK[2]}`; check_dir_name
elif [[ `grep -c '/[a-z0-9]' ${CHK[2]} 2>/dev/null` -gt 1 ]]; then clear
echo -e "${MSG[2]}"; cat -n ${CHK[2]}; echo -ne "${SLCT[2]}"
read -p "" num; DIR_NUM=`cat -n ${CHK[2]} | grep ".* $num"|awk '{print $2}'`
if [[ $num == "" ]]; then unset DIR_NUM; select_dir_name
elif [[ -d $DIR_NUM ]]; then EDIT_DIRS="$DIR_NUM"; check_dir_name; else check_dir_name; fi
elif [[ `grep -c '/[a-z0-9]' ${CHK[2]} 2>/dev/null` -eq 0 ]]; then check_dir_name; fi; }
find_dir_name() { if [[ ! -z $EDIT_DIRS ]]; then
if [[ `echo $EDIT_DIRS |grep '^/'` != "" ]] && [[ `ls $EDIT_DIRS 2>/dev/null` ]] && [[ -d $EDIT_DIRS ]]; then 
export BASE_NAME=`basename $EDIT_DIRS`; check_dir_name
else find / 2>/dev/null -name "$EDIT_DIRS" -exec echo {} >${CHK[1]} \;
if [[ `grep -c '/[a-z0-9]' ${CHK[1]}` -eq 1 ]]; then
if [[ -d `cat ${CHK[1]}` ]]; then EDIT_DIRS=`cat ${CHK[1]}`; check_dir_name
else check_dir_name; fi; else select_multi_dir; fi; fi; else select_dir_name; fi; }
select_dir_name() { if [[ ! -z "$1" ]]; then EDIT_DIRS=`echo $1 | sed "s/'//g"`
find_dir_name; else clear; echo -ne "${SLCT[1]}"; read -p "" EDIT_DIRS
EDIT_DIRS=`echo $EDIT_DIRS | sed "s/'//g"`
find_dir_name; fi; }
dialog_color() { CK=`tput bold; tput setaf 88`
CM=`tput bold; tput setaf 90`
CY=`tput bold; tput setaf 35`
CS=`tput bold; tput setaf 186`
CB=`tput bold; tput setaf 251`
CL=`tput bold; tput setaf 37`
CT=`tput bold; tput setaf 130`
CC=`tput bold; tput setaf 122`
CX=`tput bold; tput setaf 83`
CF=`tput sgr 0`; }
dialog_messages() { dialog_color
RCSS[1]="$CM\n ==>"
RCSS[2]="$CM\n ====>"
SLCT[1]="\n${RCSS[2]} ${CC}Düzenlemek istediğiniz dizinin ismini girin :${CX} "
SLCT[2]="\n${RCSS[2]} ${CC}Düzenlemek istediğiniz dizinin numarasını girin :${CX} "
SLCT[3]="\n${RCSS[2]} ${CC}Yeniden seçmek için ${CY}[Enter] ${CC}çıkış için ${CY}[c] ${CC}tuşunu \
kullanabilirsiniz :${CX} "
SLCT[4]="\n${RCSS[2]} ${CC}Yeni yetki kodunu seçin :${CX} "
SLCT[5]="\n${RCSS[2]}  ${CC}Sahipliğini belirtin  :${CX} "
SLCT[6]="\n${RCSS[2]} ${CC}Dosya ve dizinlerin mevcut ayarlarını görmek istiyormusunuz? ${CY} [e/h] ${CX}"
SLCT[7]="\n${RCSS[2]} ${CC}Yeni bir seçim için ${CY}[y]${CC} çıkış için herhangi bir tuşu kullanabilirsiniz :${CX} "
SLCT[8]="\n${RCSS[2]} ${CC}Mevcut ayarları görmek istiyormusunuz? ${CY} [e/h] ${CX}"
SLCT[9]="\n${RCSS[2]} ${CC}Ayarlar sadece dizinlere uygulansın ${CY} [ 1 ] ${CC} \n${RCSS[2]} ${CC}Ayarlar \
sadece dosyalara uygulansın ${CY} [ 3 ] ${CC} \n${RCSS[2]} ${CC}Ayarlar seçimin tamamına uygulansın ${CY} \
[ 5 ] ${CC} \n${RCSS[2]} ${CC}İptal için ${CY}[ h ]${CC} veya seçim numarası girin :${CX} "
SLCT[10]="\n${RCSS[2]} ${CC}Devam etmek için ${CY}[ enter ]${CC} tuşuna basın : \n${CX}"
SLCT[11]="\n${RCSS[2]} ${CC}Seçili işlem süreci tamamlandı..\n\n ${CC}Değişiklikleri görmek için ${CY}[ b ]${CC} \
çıkış için herhangi bir tuşu kullanabilirsiniz.. :${CX} "
MSG[0]="${RCSS[1]} ${CK}Bu betiği root haklarıyla çalıştıramazsınız !.\n\n${CF}"
MSG[1]="\n\n ${CS}Bu betik, belirli bir dizine ve o dizine ait tüm alt bölümlere\n\n toplu yetki düzenlemesi yapmak \
için hazırlanmıştır..\n\n${CK}"
MSG[2]="\n ${CS}Bu isimde birden çok dizin bulundu. Yeni bir dizin seçmek için,\n\n numara bölümünü \
boş bırakarak ${CY}[enter]${CS} ile üst menüye dönebilirsiniz\n\n${CB}"
MSG[3]="${CT}geçerli bir dizin ismi değil"
MSG[4]="${CK}dizini bu şekilde toplu bir yapılandırmaya uygun değil\n\n \
Bu seçim bilgisayarınızda geri dönüşü olmayan kalıcı hasarlar verebilir !. \n"
MSG[5]="\n${RCSS[1]} ${CL}Dizin bilgileri alınıyor .....\n\n"
MSG[6]="${CY}dizini seçildi\n${CM}${RCSS[1]} ${CY}İçerik :"
MSG[7]="${CY}dizin"
MSG[8]="${CY}dosya"
MSG[9]="${CY}kısayol  topl."
MSG[10]="${CY}bölüm"
MSG[11]="\n\n ${CT}Yetki düzenlemesini için aşağıda birkaç örnek bulunmakta ,\n\n  777 = rwxrwxrwx \t 755 = \
rwxr-xr-x \t 700 = rwx------\n  666 = rw-rw-rw- \t 644 = rw-r--r-- \t 600 = rw-------\n\n ${CS}Yetki seviyesi \
belirlemeden devam etmek isterseniz,\n\n seçimi boş bırakarak ${CY}[enter]${CS} ile devam edebilirsiniz \n\n"
MSG[12]="${CL}dizininin mevcut durumu :"
MSG[13]="${CT}geçerli bir değer değil\n"
MSG[14]="${CY}yetki düzeyi"
MSG[15]="${CB}boş${CY} bırakıldı"
MSG[16]="${CY}olarak seçildi"
MSG[17]="\n\n ${CS}Sahiplik durumunu aşağıdaki listeye göre, \
şu örneklere benzer şekilde yapabilirsiniz. \n\n Örnek :  ${CT}root:root , root:avahi , \
\$USER:root  ${CS}veya${CT}  \$USER:users  ${CS}vb"
MSG[18]="\n ${CS}GRUP LİSTESİ  ="
MSG[19]="\n\n ${CS}Dosya sahiplik haklarını belirleyin. Sahiplik haklarını belirlemeden -\n\n devam etmek \
isterseniz, seçimi boş bırakarak ${CY}[enter]${CS} ile devam edebilirsiniz\n"
MSG[23]="${CY}sahipliği"
MSG[24]="${CT}geçerli bir girdi değil \n\n"
MSG[25]="\n\n ${CK}Sisteminizin güvenliği için,"
MSG[26]="üzerinde bulunan\n\n bölümlerin içeriğini, sadece görüntüleyebilirsiniz.\n\n Bu bölüm altında \
toplu düzenleme yapılamaz."
MSG[27]="\n\n ${CK}Seçiminiz geçerli değil !"
MSG[28]="\n ${CS}Değişiklik durumunda düzenleme bilgileri, aşağıda ki dosyaya yazılacaktır !\n\n"
MSG[29]="\n ${CK}Düzenleme sırasında bazı hatalar oluştu.. \n\n \
Bu tür hatalar genellikle dosya isimlerinde ki boşluklardan kaynaklanır.\n\n"
MSG[30]="\n${RCSS[1]} ${CY}Ayarlarınız Uygulanıyor. Bekleyin ....\n"
MSG[31]="${RCSS[1]} ${CB}Son işlem tarihi : "; }
if [[ `id -u` != 0 ]]; then clear; dialog_messages; echo -e "${MSG[1]}"; sudo_allowed 'echo' &> /dev/null
clear; select_dir_name $1; else echo -e "${MSG[0]}"; sleep 2; exit 1; fi
tput sgr 0
